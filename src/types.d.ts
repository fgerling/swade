declare interface Roll {
  options: any;
}

declare interface Scenes {
  active: any;
}

declare interface BaseEntityData {
  data: any;
}

declare interface Die {
  formula: any;
}
